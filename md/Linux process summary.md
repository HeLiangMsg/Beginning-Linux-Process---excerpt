##总结
最后来个总结吧，两周的时间去完成Linux程序设计的功能。
包含：进程和信号，线程，管道，信号量、共享内存和消息队列，套接字。

###进程和信号
1. 启动进程的三种方式，
	- 在程序内部启动一个程序：system	` int system(const char *command);`
	- 替换进程映像：exec函数		` int execlp(const char *file, const char *arg, ...);		int execlp("ps","ps","aux",);` 再使用函数中包含数组型参数时，需要使用NULL来结尾，0不行。使用NULL做一下结尾，否则程序在运行时找不到下一个运行的地址下标。
	- 复制进程映像：fork	` pid_t fork(void);`
2. 等待一个进程：wait
wait系统调用将暂停父进程知道它的子进程结束为止。返回值为子进程的PID `pid_t wait(int *status);`

3. 信号
	- 信号定义：信号是UNIX和Linux系统响应某些条件而产生的一个事件
	- 信号处理：使用`sigacton`函数来处理信号，函数原型：`int sigaction(int sig, const struct sigaction *act, struct sigaction *oact);`一个sigaction的结构体至少包含以下结构成员：
		- void (*) (int) sa_handler		/* 函数指针，用于接受到信号sig后的处理函数 */
		- sigset_t sa_mask			/* 信号集，该信号集在调用信号处理函数之前，被加到进程的屏蔽字中。里面是一组将被堵塞且不会传递给该进程的信号 */
		- int sa_flags		/* 信号行为的改变，可以通过设置SA_RESETHAND来实现，二次signal调用时对信号处理进行重置操作 */
	- 简单调用时，设置act结构体的值，ocat置0
4. 信号集
	- 略

###线程
1. 什么是线程
	- 在一个程序中的多个执行路线就叫线程。更准确的说：线程就是一个进程内部的一个控制序列。
	- 当在进程中创建一个新线程时，新的执行线程将拥有之间的栈（自己的局部变量），但与它的创建者共享**全局变量、文件描述符、信号处理函数和当前目录的状态**
2. 线程的优缺点
	- 略
3. 第一个线程程序
	- 绝大多数函数名以*pthread_*开头，并且在编译程序时需要使用选项-lpthread来链接程序
	- 在一个多线程程序中，只有一个errno的变量供所有线程使用。类似的资源还有，fputs之类的函数，这些函数通常用一个全局性区域来缓冲输出数据
	- 函数pthread_create，它的作用是创建一个新线程。原型：`int pthread_create(pthread_t * thread, pthread_attr_t * attr, void *(*start_routine)(void *), void * arg)`
		- arg1：这个指针指向的变量将被写入到一个标识符中，使用这个标识符来引用新线程。
		- arg2：线程的属性，一般不需要设置，直接赋值为NULL
		- arg3和4：线程要启动执行函数和传递给函数的参数
	- 线程通过pthread_exit函数终止执行。原型：`void pthread_exit(void *retval);`
	- pthread_join函数作用等待子线程的结束。原型：`void pthread_join(pthread_t th, void **thread_return);`thread_return的值等于pthread_exit函数中retval的值。
4. 信号量
信号量是一个特殊类型的变量，它可以被增加或被减少，但操作上被保证是原子性的操作。即同时只有一个线程去操作它。通过信号量的计数来保证任一时刻只有一个线程可以访问被它包含的共享资源。
	- 函数sem_init用于初始化一个信号量。原型：
	- `int sem_init(sem_t *sem, int pshared, unsigned int value);`这个函数初始化sem指向的信号量对象，pshared参数控制信号量的类型，value参数是信号量的值。如果这个值小于0则访问它的值将被堵塞。
	- `int sem_wait(sem_t *sem);`和`int sem_post(sem_t *sem);`两个函数用于控制信号量的值。这两个函数都以初始化后的信号量对象的指针为参数。前者将以原子操作的方式将信号量减1，但它只有等待信号量为非零值才会这么做。后者同样以原子操作的方式给信号量的值加1
	- 最后一个`int sem_destroy(sem_t *sem)`,信号量的删除函数。如果试图删除的信号量正在被其它一些线程等待，就会收到一个错误。
	- 与大多Linux函数一样，这些函数的成功返回值都为0，并且会设置errno的变量

5. 互斥量
另外一种在多线程中同步访问的方法是使用互斥量。它允许程序员锁住某个对象，使得每次只能有一个线程访问它。为了控制对某个共享资源的访问，必须在操作资源的代码之前锁住一个互斥量，在完成后一定要解锁这个互斥量。
函数如下，类似与信号量：
```
int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr);
int pthread_mutex_lock(pthread_mutex_t *mutex);
int pthread_mutex_unlock(pthread_mutex_t *mutex);
int pthread_mutex_destroy(pthread_mutex_t *mutex);
```
函数调用成功返回0，失败返回错误码，但未设置errno。注意在一个线程中对一个互斥量进行两次或以上的加锁操作，这样在本线程中，不能解锁。其它线程也因无法加锁互斥量而堵塞，遂而形成死锁。

6. 线程属性
线程可以被**脱离，调度，取消**。通过设置线程的属性可以做到这一点。

7. 多线程
当我们一次性使用循环的方式创建一个数组的线程。在pthread_create函数中的最后一个选项中，将数组的下标作为参数传递到子线程中当参数时。由于主线程的运行足够快，就可能会改变某些线程的参数对共享变量和多个执行路径没有做到足够重视，程序就有可能出现错误。
错误为，当主线程不断的执行*pthread_create*函数，在子线程中打印传进来的参数就发生了不一致的变化。以下为一个简单的解决方法。
```
 for (lots_of_threads = 0;lots_of_threads < NUM_THREAD; lots_of_threads++){
            //使用fakelock(全局变量)来查看子线程是否创建完成
            while(!fakelock) {
            }
            pthread_mutex_lock(&mutex);
            fakelock = 0;
            pthread_mutex_unlock(&mutex);
            res = pthread_create(&(a_thread[lots_of_threads]),NULL,thread_function,&lots_of_threads);
            if (res != 0){
                char * str;
                sprintf(str,"[%d] pthread_create failure",lots_of_thr：eads);
                perror(str);
                exit(EXIT_FAILURE);
            }
    }
```
错误的情况，如下
	```
thread_function is running. Argument was 3
thread_function is running. Argument was 5
thread_function is running. Argument was 2
Waiting for threads to finish...
thread_function is running. Argument was 1
thread_function is running. Argument was 4

	```
改善后的情况
	```
hread[1] is running
Bye Form 1
thread[2] is running
Bye Form 2
thread[3] is running
Bye Form 3
thread[4] is running
Bye Form 4
thread[5] is running
Bye Form 5
Waiting for threads to finish...
thread[6] is running
Bye Form 6
All done

	```

###进程间通信：管道
1. 什么是管道？
当从一个进程连接数据流到另一个进程使，中间的媒介称为管道。管道只能是单向的，通常我们把一个进程的输出通过管道流向另一个进程的输入。

2. 进程管道
使用管道在两个进程之间传递数据最简单的方式就是使用popen和pclose函数。opopen的函数需要的是一个与打开模式成对的命令。

3. pipe调用
pipe函数创建两个文件描述符，数据向fileDescriptor【1】写入，从fileDescriptor【0】读出。数据基于先进先出的处理方式。

4. 管道关闭之后的操作
对于管道，通常不知道有多少数据可以读，往往采用轮询的方式。当没有数据可以读时，read调用将被堵塞。如果管道的另一端关闭时，read调用返回的是0而不是堵塞。fork产生的子进程也会用户和父进程相同的一对管道描述符，所以在父子进程使用管道通信时，要提前关闭对应的一端管道描述符。

5. 把管道用作标准输入和标准输出
使用fork创建子进程，然后子进程使用close（0）关闭标准输入，然后使用dup（file_pipes[0]）复制管道的输出端的描述符。接着关闭子进程中的管道描述符的输入输出端。此时子进程中程序的标准输入就是管道的标准输出间接的就是父进程中的标准输入端。

6. 命名管道
因为FIFO中没有数据，所以当调用cat和echo单独读或写这个管道时，都会被堵塞，cat等待数据的到来，而echo等待其它进程来读取数据。原因：可以与命名管道的大小有关，如果使用`ls -l /tmp/my_fifo`，会发现这个管道的大小为0。
与通过pipe调用创建管道不同，**FIFO是以命名文件的形式存在，**而不是打开的文件描述符，**所以在对它进行读写操作之前必须先打开它，FIFO也用open和close函数打开和关闭。**
使用open打开FIFO文件的几种方式：
	- `open(const char * path,O_RDONLY)`在这种情况下，open调用将阻塞，除非有一个进程以写方式打开同一个FIFO，否则它不会返回。
	- `open(const char* path,O_RDONLY |O_NONBLOCK))`：即使没有其他进程以写方式打开FIFO，这个open调用已将成功并立刻返回。
	- `open(const char * path,O_WRONLY)`：这种情况下open调用也会堵塞，直到有一个进程以读方式打开同一个FIFO为止。
	- `open(const char * path,O_WRONLY|O_NONBLOCK)`：这个函数调用总是立刻返回，但如果没有进程以只读的方式打开FIFO文件，open调用将返回一个错误-1并且FIFO也不会被打开。如果确实有一个进程以只读方式打开FIFO文件，那么我们就可以通过它返回的文件描述符对这个FIFO文件进行写操作。
	- 如果没有进程以读方式打开管道，非阻塞写方式的open调用将失败，但非阻塞读方式的open调用总是成功，close调用的行为并不受O_NONBLOCK标志的影响。
对FIFO进行读写操作的情况如下：
	对于一个空的、堵塞的FIFO的read调用将等待，直到有数据可以读时才进行执行。与此相反，对一个空的、非堵塞的FIFO的read调用将立刻返回0字节。
    对于一个完全堵塞FIFO的write调用将等待，直到数据可以被写入（即管道的另一端被打开时）才可以继续执行。
    提供的程序两个程序使用的都是阻塞模式的FIFO。我们首先启动写进程（以写堵塞的方式打开管道），它将堵塞以等待读进程打开这个FIFO。读进程启动后，写进程解除堵塞并开始向管道写数据。同时，读进程也开始从管道中读取数据。

###信号量、共享内存和消息队列
IPC（Inter-Process Communication,进程间通信）机制
信号量： 用于关联对资源的访问
共享内存： 用于在程序之间高效地共享数据
消息队列： 在程序之间传递数据的一种简单方法
####IPC信号量
用于管理对资源的访问。为了防止出现多个程序同时访问一个共享资源而引发的问题，我们需要有一种方法，它可以通过生成并使用令牌来授权，在任一时刻只能有一个执行线程访问代码的临界区域。
IPC中的信号量和POSIX中的信号量
> 在12章所讨论的，信号量是一个特殊的变量，它只取正整数值，并且程序对其访问都是原子操作。在本章中，我们将对这个较早的简化定义做出进一步的解释。

信号量的一个更正式的定义是：它是一个特殊变量，只允许对它进行P操作和V操作，其中
- P（sv）：如果sv的值大于零，就给它减1；如果等于零，就挂起该进程的执行状态。
- V（sv）：如果有其他进程因等待sv而被挂起，就让它恢复运行；如果没有进程因为等待sv而被挂起，就给它加1

理论操作：
	两个进程共享信号量变量sv。一旦其中一个进程执行了P（sv）操作，它将得到信号量，并可以进入临界区域（独占式的执行代码区域，在这个区域中只有一个执行进程）。而第二个进程将被阻止进入临界区域，因为当它试图进行P（sv）操作时将被挂起，等待第一个进程离开临界区域并执行V（sv）操作释放信号量。

三个函数：
 - semget用于创建一个新的信号量或取得一个已有信号的键。` int semget(key_t key, int nsems, int semflg); `键，信号量个数，创建类型*向创建文件一样，0666|IPC_CREAT*
 - semop用于改变信号量的值` int semop(int semid, struct sembuf *sops, unsigned nsops);`
 	```
    struct sembuf {
 			unsigned short sem_num;  /* semaphore number */  除非操作一组信号量，否则为0
            short          sem_op;   /* semaphore operation */ 操作值
            short          sem_flg;  /* operation flags */  IPC_NOWAIT and SEM_UNDO.
		};
    ```
 - semctl用于初始化或删除信号量
 - `int semctl(int semid, int semnum, int cmd, ...);` id，个数，参数4依赖于，参数三。例如：
```
/*添加信号量*/
union semun sem_union;
sem_union.val = 1;
semctl(sem_id,0,SETVAL,sem_union)
/*删除信号量*/
union semun sem_union;
semctl(sem_id,0,IPC_RMID,sem_union)
```
 - 我们需要特别小心，不要在无意之中在程序执行结束后还留下信号量未删除。它可以会在你下次运行次程序时引发问题，而且信号量也是一种有限的资源，需要节约使用。
感觉：信号量一脸懵逼。NO，信号量就是向系统申请构建一个资源，用于不同的进程的共享。然后通过操作这个资源的标记，来判断资源归属那个进程调用。


####共享内存
共享内存是3个IPC机制只的第二个，它允许两个不相关的进程访问同一个逻辑内存。共享内存是在两个正在运行的进程之间传递数据的一种非常有效的方式。但是它未提供同步机制，所有我们通常需要用其它的机制来同步对共享内存的访问。
四个函数：
 - shmget用于创建一个共享内存	`int shmget(key_t key, size_t size, int shmflg);`
 - 第一次创建共享内存段时，它不能被任何进程访问。要想启用对共享内存的访问，必须将其连接到一个进程的地址空间中，这项工作由shmat函数来完成。`void *shmat(int shmid, const void *shmaddr, int shmflg); `共享地址id，共享内存地址（如果NULL，则有系统分配），共享内存选项。成功返回地址，失败返回（void*）-1,并设置errno
 - shmdt函数的作用是将共享内存从当前进程中分离	，但并未删除，只是使共享内存对当前进程不可在用。`int shmdt(const void *shmaddr);`成功返回0，失败-1，并设置errno
 - shmctl共享内存控制函数，可以用于共享内存的删除。`int shmctl(int shmid, int cmd, struct shmid_ds *buf); ` 共享内存id，cmd：如IPC_RMID,最后参数是一个指针，它指向包含共享内存模式和访问权限的结构。NULL

####消息队列
1. 什么是消息队列
**消息队列**是一种进程间通信或同一进程的不同线程之间的通信方式，消息队列提供一种机制，使得一个进程可以发送消息到一个队列中，然后另一个进程从此队列中取出数据，与管道不同的时，消息队列依附于内核，即可以让两个进程不必同时存在——在一个进程发送一个消息后退出，另外一个进程可以在数天之后获得。前提：共享内存资源不被destroy。

2. 优缺点
消息队列本身是异步的，它允许接收者在消息发送很长时间后在取回数据。但消息队列的异步特点，也造成了一个缺点，就是接收者必须轮询消息队列，才能收到最近的消息。*而共享内存则是会接受到最新的消息，但没有异步机制*,和信号相比，消息队列能够传递更多的信息。与管道相比，消息队列提供了有格式的数据，这可以减少开放人员的工作量。但消息队列仍然有大小限制。

3. 使用
msgget函数来创建和访问一个消息队列:`int msgget(key_t key, int msgflg);`  访问同一个消息队列的key值，msgflg为文件的权限标志在加一个IPC_CREAT标志，成功返回正整数，即队列标志符，失败返回-1.
msgsnd函数用来将消息添加到消息队列中:`int msgsnd(int msgid, const void *msg_str, size_t msg_sz, int msgflg);`
	- 消息的结构收到两方面的约束。首先，它的长度必须小于系统规定的上限；其次，它必须以一个长整形成员变量开始，接收函数将用这个成员变量来确定消息的类型。建议将消息类型定义为:
	```
	struct my_message {
    	long int message_type;　/* message type, must be > 0*/
        /* content */
    };
    ```
    - 参数的分析：msgid，msgget的返回值，msg_str是准备发送消息的指针。msg_gz消息长度，不包括长整形变量的大小。msgflg控制在当前消息队列满或队列消息到达系统范围的限制时将要发送的事情。成功返回0失败返回-1并设置errno

msgrcv函数从一个消息队列中获取消息:`int msgrcv(int msgid, void *msg_ptr, size_t msg_size, long int msgtype, int msgflg)` 	前三个函数和msgsnd相同，msgtype参数表明要接受队列中的和msgtype类型相同的消息。如果为0则读取队列中的第一个可用消息，如果这个值大于0，则获取具有相同数据类型值的第一个消息。如果小于0，将获得消息类型等于或小于msgtype的绝对值的第一个消息。msgflg，用于控制队列中没有相同类型的消息可以接收时而发生的事情。

msgctl函数，作用与共享内存的控制函数非常相似:`int msgctl(int msgid, int command ,struct msgid_ds *buf);` arg1：msgget的返回值。command是将要采取的动作：IPC_RMID 删除消息队列，arg3：定义了当前消息队列的有关信息。如：消息队列的权限，发送时间，接受时间等等。

**在编写消息队列时遇到的问题:**
1. 权限不足
查看了自己的参数是正确，最后换了一个key_t的值，问题解决。原因为了方便，直接使用的自定义数字。
2. 非法参数。
	- 在msgsnd时，Invalid argument，查看参数，long mtype的值必须大于0，消息的长度为实际发送数据的大小，不包含long type的大小。
	- 在msgrcv时，同样消息的长度要确定。最后一个参数不知道怎么用就不要使用，补0

3. 数值交互
使用sigsnd发送指定类型数据时，需要指定long type的值，且值必须大于0.
使用sigrcv接受指定类型数据时，也需要指定long type的值。值任任意。可正可负，可为0
4. 最后一点，当消息队列被删除时，数据也就清空了，所以消息队列只用于短时间的数据交流

####IPC状态命令
IPC机制一个让人烦恼的问题：编写错误的程序或因为某些原因而执行失败的程序将把它的IPC资源遗留在系统中，并且这些资源在程序结束后很长时间仍留在系统。这将导致对程序的新调用执行失败，因为程序期望以一个干净的系统来启动。IPC状态命令（ipcs）和删除命令（ipcrm）提供了一种检测和情理IPC机制的方法。
- 使用`ipcs`会输出共享内存，信号量，消息队列的全部资源的状态
- 对应选项的`-s -m -q`分别对应着信号量，共享内存，消息队列的输出。
- 还有一个选项`-p`输出资源对应的PID
- 输出共享资源对应的执行程序
```
	ps -p `ipcs -mp |  tr -s ' ' | cut -d ' ' -f 3| sort | uniq | tr -d 'A-z' | tr '\n' ' '`
```

###套接字
1. 什么是套接字？
套接字是一种通信机制，凭借这种机制，客户/服务器系统的工作即可以在本地单机上工作，也可以跨网络进行。
套接字和管道相比，同样是读写类似文件描述符的操作。不同的是，套接字明确的将客户和服务器分开来，可以实现多个客户连接一个服务器。

2. 套接字连接
首先，服务器应用程序使用socket来创建一个套接字，它是系统分配给该服务器进程的类似文件标识符的资源，它不能与其它进程共享。
接下来，服务器进程会给套接字起个名字。本地套接字的名字是Linux文件系统中的文件名。对于网络套接字，它的名字是与客户连接的特定网络有关的服务标识符（端口号或访问点）。这个标识符将允许Linux将进入针对特定端口号的连接转接到正确的服务器进程。例如：Web服务器一般在80端口上创建有关套接字，这是一个专用于此目的的标识符。Web浏览器知道对于用户想访问的Web节点，应该使用端口80来建立HTTP连接。
我们用系统调用**bind**来给套接字命名(关联于本地某个文件)然后服务器进程就开始等待客户连接这个命名套接字。系统调用**listen**的作用是，创建一个队列并将其用于存放来自客户的请求连接。服务器通过系统调用**accept**来接受客户的连接。服务器调用**accept**时，它会创建一个与原来的命名套接字不同的新套接字。这个套接字只用来与这个特定客户进行通信，而命名套接字则被保留下来继续处理来自其它客户的连接。
基于套接字系统的客户端更加简单。客户首先调用socket创建一个未命名套接字，然后将服务器的命名套接字作为一个地址（或标识符）来调用connect与服务器建立连接。
一旦连接建立，我们就可以想使用底层的文件描述符那样用套接字来实现双向的数据通信。

3. 套接字属性
套接字的特性有3个属性确定，它们是：域（domain）、类型（type）和协议（protocol）
	1. 套接字的域
        域指定套接字通信中使用的通信流的方式。
        - 最常见的套接字域是AF_INET,它指的是Internet网络。许多Linux局域网使用的都是此网络。
        - 还有一个域是UNIX文件系统域AF_UNIX,这个域的底层协议就是文件输入/输出，而它的地址就是文件名。当运行这个程序时，就可以在当前目录下看到这个地址。

	> 客户可以通过IP端口来指定一台机器上某个特定服务。在系统内部，端口通过分配一个唯一的16位的整数来标识，在系统外部，则需要通过IP地址和端口号的组合来确定。套接字作为通信的终点，它必须在开始通信之前绑定一个端口。知名的服务通常有一些特定端口，比如ftp（21）和httpd（80）等。在选择端口时不要随意选择以避免端口被占用的情况。一般情况下，小于1024的端口号都是为系统服务保留的。

	2. 套接字类型
一个套接字域可能有多种不同的通信方式，而每种通信方式又有其不同的特性。但AF_UNIX域的套接字没有这样的问题，它提供了一个可靠的双向通信路径。在网络域中，我们就需要注意底层网络的特性，以及不同的通信机制是如何受到它们的影响。
因特网提供了两种通信机制：流（stream）和数据包（datagram）。他们有着截然不同的服务层次。
		- 流套接字
	流套接字（在某些方面类似于标准输入/输出流）提供的是一个有序、可靠、双向字节流的连接。*ps：使用TCP中的序号机制保证大的消息将被分片、传输、再重组,数据在到底目的地时不发生数据乱包*这很像一个文件流，它接受大量的数据，然后以小数据块的形式将它们写入底层磁盘。
            流套接字由底层SOCK_STREAM指定，它们是在AF_INET域中通过TCP/IP连接实现。
            > TCP/IP代表的是传输控制协议（Transmission Control Protocol）/网际协议（Internet Protocol）。IP协议是针对数据包的底层协议，它提供一台计算机通过网络到达另一台计算机的路由。TCP协议提供排序、流控和重传，以保证大数据的传输可以完整地到达目的地或报告一个适当的错误条件。
		- 数据报套接字
	与流套接字相反，由类型SOCK_DGRAM指定的数据报套接字不建立和维持一个连接。它对可以发送的数据报的长度有限制。数据报作为一个单独的网络消息被传输，它可能丢失、复制或无序到达。
	数据报套接字在AF_INET域中通过UDP/IP连接实现，它提供的是一种无序的不可靠服务。但从资源的角度来看，相对来说它们开销比较小，因此不需要维护网络连接。而且不需要花费时间来建立连接，所以它们的速度也很快。
	数据包适用于信息服务中的“单次（single-shot）”查询，它主要用来提供日常状态信息或执行低优先级的日志记录。服务器的奔溃不会给客户造成不便，也不会要求客户重启。
	3. 套接字协议
        暂时使用默认值。

**后面的和原socket章总结一致。**









