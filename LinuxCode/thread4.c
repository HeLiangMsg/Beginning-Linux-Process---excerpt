#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

void * thread_function(void *arg);
pthread_mutex_t work_mutex;

#define WORK_SIZE 1024
char work_area[WORK_SIZE];
int time_to_exit = 0;

int main()
{
    int res;
    pthread_t a_thread;
    void * thread_result;

    res = pthread_mutex_init(&work_mutex,NULL);
    if (res != 0){
        perror("Pthread_mutex_init failure");
        exit(EXIT_FAILURE);
    }
    res = pthread_create(&a_thread,NULL,thread_function,NULL);
    if (res != 0){
        perror("Thread_create failure");
        exit(EXIT_FAILURE);
    }
    pthread_mutex_lock(&work_mutex);
    printf("Input some text,Enter 'end' to finish\n");
    while (!time_to_exit){
        fgets(work_area,WORK_SIZE,stdin);
//        pthread_mutex_lock(&work_mutex);
        pthread_mutex_unlock(&work_mutex);

        while(1){
            pthread_mutex_lock(&work_mutex);
            if (work_area[0] != '\0'){
                pthread_mutex_unlock(&work_mutex);
                sleep(1);
            }else{
                break;
            }
        }
    }
    
    pthread_mutex_unlock(&work_mutex);
    printf(" waiting for thread to finish...\n");
    res = pthread_join(a_thread,&thread_result);
    if (res != 0){
        perror("pthread_join failure");
        exit(EXIT_FAILURE);
    }
    printf("Thread joined\n");
    pthread_mutex_destroy(&work_mutex);
    exit(EXIT_SUCCESS);

}

void * thread_function(void *arg)
{
    sleep(1);
    pthread_mutex_lock(&work_mutex);
    while (strncmp("end",work_area,3) != 0){
        printf("You input %zu character\n",strlen(work_area) - 1);//%u 是用来打印unsigned int 类型的，而sizeof的返回值为size_t的类型，此类型需要使用zu来格式化输出。
        
        work_area[0] = '\0';//释放互斥锁
        pthread_mutex_unlock(&work_mutex);
        sleep(1);//完成字符的统计，解锁，睡眠等待主线程下一步操作。
        pthread_mutex_lock(&work_mutex);
        while (work_area[0] == '\0'){
            pthread_mutex_unlock(&work_mutex);
            sleep(1);
            pthread_mutex_lock(&work_mutex);
        }
    }
    time_to_exit = 1;
    work_area[0] = '\0';//释放互斥锁
    pthread_mutex_unlock(&work_mutex);
    pthread_exit(NULL);
}

