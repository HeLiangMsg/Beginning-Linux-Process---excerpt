#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BUFSIZZ 90

int main()
{
    FILE * read_fp;
    char buffer[BUFSIZZ + 1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));
    //read_fp = popen("ps ax","r");
    read_fp = popen("cat popen3.c","r");
    if (read_fp != NULL){
        chars_read = fread(buffer,sizeof(char),BUFSIZZ,read_fp);
        while (chars_read > 0){
            buffer[chars_read -1] = '\0';
            printf("---------------Reading %d:-\n %s\n",BUFSIZZ,buffer);
            chars_read = fread(buffer,sizeof(char),BUFSIZZ,read_fp);
        }
        pclose(read_fp);
        exit(EXIT_SUCCESS);
    }
    exit(EXIT_FAILURE);
}
