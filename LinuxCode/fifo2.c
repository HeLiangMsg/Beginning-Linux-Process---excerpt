#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define FIFO_NAME "/tmp/my_fifo"

int main(int argc,char *argv[])
{
    int res;
    int open_mode = 0;
    int i;

    if (argc < 2){
        fprintf(stderr,"Usage :%s <some combination of O_RDONLY O_WRONLY O_NONBLOCK>\n",*argv);
        exit(EXIT_FAILURE);
    }
    for(i = 1; i < argc; i++){
         if (strcmp(*++argv,"O_RDONLY") == 0)
                 open_mode |= O_RDONLY;
         if (strcmp(*argv,"O_NONBLOCK") == 0)
                 open_mode |= O_NONBLOCK;
         if (strcmp(*argv,"O_WRONLY") == 0)
                 open_mode |= O_WRONLY;
    }

    printf("%d\n",open_mode);
    if (access(FIFO_NAME,F_OK) == -1){
        res = mkfifo(FIFO_NAME,0777);
        if (res != 0){
//                fprintf(stderr,"Could not create fifo %s",FIFO_NAME);
                char  str[BUFSIZ];
                sprintf(str,"Could not create fifo %s",FIFO_NAME);
                perror(str);
                exit(EXIT_FAILURE);
        }
    }//判断文件是否存在，如若不存在则创建。

   printf("process %d opening FIFO\n",getpid());
   res = open(FIFO_NAME,open_mode);//打开管道，当使用读标志来打开时，如果没有其它进程使用写标志打开时，当前进程被堵塞。
   printf("process %d open FIFO result %d\n",getpid(),res);
//   sleep(4);
   if (res != -1) close(res);
   printf("process %d close FIFO finished\n",getpid());
   exit(EXIT_SUCCESS);

}

