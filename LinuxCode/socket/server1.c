#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <sys/socket.h>
#include <stdio.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>

static int running = 1;

void server_exit()
{
	running=0;
	printf("server exit\n");
}

int main()
{
	int server_sockfd,client_sockfd;
	int server_len,client_len;
	struct sockaddr_un server_address;
	struct sockaddr_un client_address;

	//删除以前的套接字，为服务器创建一个未命名的套接字。
	server_sockfd = socket(AF_UNIX,SOCK_STREAM,0);
	if (server_sockfd == -1) {
		perror("socket");
		exit(1);
	}
//	printf("%d\n",server_sockfd);
	unlink("server_socket");

	//命名套接字
	server_address.sun_family = AF_UNIX;
	strcpy(server_address.sun_path,"server_socket");
	server_len = sizeof(server_address);
	bind(server_sockfd,(struct sockaddr*) &server_address,server_len);

	printf("%d\n",alarm(15));
	struct sigaction act;
	act.sa_handler = server_exit;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_RESETHAND;
	sigaction(SIGALRM,&act,NULL);
	//创建一个连接队列，等待客户进行连接：
	listen(server_sockfd, 5);
	while(running) {
		char ch;
		printf("server waiting\n");

		/*目标：实现服务器端的不堵塞行为，那就要设置服务器的套接字的属性，
			 * 实现第一步：获得这个文件的原有属性，
			 * 第二步：增添这个文件的新属性。
		 * */
		int flags = fcntl(server_sockfd, F_GETFL,NULL);
		fcntl(server_sockfd, F_SETFL,O_NONBLOCK | flags);
		//接受一个连接
		client_len = sizeof(client_address);
		client_sockfd = accept(server_sockfd,
				(struct sockaddr*) & client_address,&client_len);
		
		printf("NONBLOCK\n");

		read(client_sockfd,&ch,1);
		ch++;
		write(client_sockfd,&ch,1);
		close(client_sockfd);
		sleep(1);
		//使用alram 定时发送一个alarm信号，再使用sigaction来扑捉并处理这个信号，处理函数的内容为：exit（0），或者使用一个修改while（running）的值。
		//不堵塞的问题解决了，开始干。


	}


}
