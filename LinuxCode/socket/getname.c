#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc,char *argv[])
{
	char * host, **names, **addrs;
	struct hostent *hostinfo;


	if (argc == 1 ){
		char myname[256];
		if (gethostname(myname, 255) != 0 ) {//得到一个以NULL为结尾的字符串数组，存放于第一个参数中，第一个参数为大小。
			perror("gethostname");
			exit(1);
		}
		host = myname;
		printf("host name is [%s]\n",host);
	}
	else {
		host = argv[1]; //参数要的是域名，+_+ 
		printf("argv[1] is :[%s]\n",argv[1]);
	}
	/*
	 * 把host变量设置为程序所提供的命令行参数，或默认设置为用户主机的主机名。
	 * */

	
	hostinfo = gethostbyname(host);//这个函数会查看/etc/hosts文件中的内容。和 /etc/services

	if (!hostinfo) {
		fprintf(stderr,"cannot get info the host: %s\n",host);
		exit(1);
	}
	/*
	 * 调用gethostbytname，如果未找到相应的信息就报告一条错误。
	**/

	printf("results for host [%s]:\n",host);
	printf("Name: [%s]\n", hostinfo -> h_name);

	names = hostinfo -> h_aliases;
	if (*names == NULL) {
		printf("No Aliases\n");
	}
	else {
			
		while (*names) {
			printf(" [Aliases is :%s]\n",*names);
			names ++;
		}
	}
	/*
	 * 打印主机名和它可能有的所有别名
	 * */


	if (hostinfo -> h_addrtype != AF_INET ) {
		fprintf(stderr,"not an IP host!\n");
		exit(1);
	}
	/*
	 * 如果查询的主机不是一个IP主机，就发出警告并退出
	 * */

	addrs = hostinfo -> h_addr_list;
	while (*addrs) {
		printf("addr_list is  [%s]", inet_ntoa(*(struct in_addr *) *addrs));
		addrs ++;
	}
	/* 
	 * 显示它的所有IP地址
	 * */
	printf("\n");
	exit(0);
}








