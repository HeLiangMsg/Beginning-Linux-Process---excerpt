#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>

#include <netinet/in.h>
#include <arpa/inet.h>


int main()
{
	int server_sockfd,client_sockfd;
	int server_len,client_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;

	//删除以前的套接字，为服务器创建一个未命名的套接字。
	server_sockfd = socket(AF_INET,SOCK_STREAM,0);
	if (server_sockfd == -1) {
		perror("socket");
		exit(1);
	}
	printf("%d\n",server_sockfd);
	unlink("server_socket");

	//填充网络套接字结构体内容并关联此套接字至文件系统上面的一个文件名
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
	server_address.sin_port = 3366;

	server_len = sizeof(server_address);
	bind(server_sockfd,(struct sockaddr*) &server_address,server_len);

	//创建一个连接队列，等待客户进行连接：
	listen(server_sockfd, 5);
//	while(1) {
		char ch;
		printf("server waiting\n");
		//接受一个连接
		client_len = sizeof(client_address);
		sleep(2);
		client_sockfd = accept(server_sockfd,
				(struct sockaddr*) & client_address,&client_len);
		printf("Welcome '%d'\n",client_address.sin_addr.s_addr);
		
		read(client_sockfd,&ch,1);
		ch++;
		write(client_sockfd,&ch,1);
		close(client_sockfd);
//	}


}
