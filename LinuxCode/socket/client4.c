#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>

#include <netinet/in.h>
#include <arpa/inet.h>


int main()
{
	int sockfd;
	int len;
	struct sockaddr_in address;
	int result;
	char ch = 'R';

	//为客户创建一个套接字
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	//根据服务器的情况给套接字命名：
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr("127.0.0.1");
	address.sin_port = htons(3366);

	len = sizeof(address);
	//将我们创建的套接字连接到服务器的套接字
	result = connect(sockfd,(struct sockaddr * )&address,len);//强转，将AF_INET对应的结构体转化为，通用的套接字结构体。

	if (result == -1) {
		perror("client connect");
		exit(1);
	}

	//开始读写sockfd
	write(sockfd, &ch, 1);
	read(sockfd,&ch,1);
	printf("char from server = %c\n",ch);
	sleep(10);

	/*
	write(sockfd, &ch, 1);
	read(sockfd,&ch,1);
	printf("char from server = %c\n",ch);
*/
	close(sockfd);

	exit(0);

}
