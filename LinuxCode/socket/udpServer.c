#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>



int main()
{
	int sockfd;
	struct sockaddr_in addr;
	int result,len;

	char buff[512];

	/* create socket */
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd == -1) {
		perror("socket");
		exit(1);
	}


	/*init strct sockaddr_in ,and bind sockfd*/
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(2255);

	len = sizeof(addr);

	result = bind(sockfd, (const struct sockaddr*)&addr,len);
	if (result == -1) {
		perror("bind");
		exit(1);
	}

	while (1) {
		result = recvfrom(sockfd, buff, len, 0, (struct sockaddr*)&addr, &len);//现在遇到的问题就是，这个套接字的地址怎么填写，
		if (result == -1) {
			perror("bind");
			exit(1);
		}
		buff[result] = '\0';
		printf("%s\n",buff);
		if (strncmp(buff,"end",3) == 0) {
			break;
		}
		sleep(6);
		result = sendto(sockfd, buff, len, 0, (const struct sockaddr*)&addr,len);
	}







	exit(0);
}
