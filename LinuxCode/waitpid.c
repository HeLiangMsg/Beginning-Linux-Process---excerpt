#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
        pid_t pid;
        char * message;
        int n;
        int result;
	int status;

        printf("fork program starting \n");
        pid = fork();
	/*
	 * 目标：
	 * 1.在子进程中睡眠5s
	 * 2.在父进程中等待子进程的退出，使用waitpid函数
	 * */
	switch (pid) 
	{
		case 0:
			printf("this is child.\n");
			sleep(5);
			break;
		case -1:
			perror("fork");
			exit(1);
			break;
		default:
			printf("this is parent.\n");
			break;
		
	}

	if (pid != 0) {
		printf("wait child porcess...\n");
		result = waitpid(pid,(int*)0, WNOHANG);
		if (result < 0) {
			perror("waitpid");
			exit(1);
		}
		printf("child process finshd,pid:%d,result:%d\n",pid,result);
	}
	if (pid == 0) {
		printf("child process exit\n");
	}

        exit(0);

}
