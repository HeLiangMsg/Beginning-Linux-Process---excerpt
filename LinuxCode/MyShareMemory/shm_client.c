#include <stdio.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>

#include <stdlib.h>

#define SHMSZ 27

int main()
{
	char c;
	int shmid;
	key_t key;
	char * shm,*s;

	key = 5678;


	if ((shmid = shmget(key,SHMSZ,IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		exit(1);
	}

	printf("shmid:%d\n",shmid);

	if ((shm = shmat(shmid,NULL,0)) == (char *)-1) {
		perror("shmat");
		exit(1)	;
	}


	//读取由服务器在共享内存中写入的数据。
/*	for (c = 'a'; c<= 'z'; c++) 
		*s++ = c;

	*s = NULL;//这应该是字符串结尾的标志。
	

	while (*shm != '*')
		sleep(1);
*/

	for (s = shm; *s != '\0'; s++) 
		putchar(*s);
	putchar('\n');

	*shm = '*';

	if (shmdt(shm) < 0) {
		perror("shmdt");
		exit(1);
	}


	exit(0);
}
