#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

static int alarm_fired = 0;

void ding(int sig)
{
    alarm_fired = 1;
}

int main()
{
    pid_t pid;

    printf("alarm application starting\n");

    pid = fork();

    switch(pid)
    {
        case -1:
                perror("fork failed");
                exit(1);
                break;
        case 0:
                sleep(5);
                kill(getppid(),SIGALRM);
                exit(0);
    }

    printf("waiting for alarm to go off \n");
    (void) signal(SIGALRM,ding);

    pause();

    if(alarm_fired){
        printf("Ding!\n");
    }
    printf("Done\n");

    exit(0);
}
/**
 *首先在main函数中fork一个子进程，在子进程中等待5s后发送一个SIGALRM信号给它的父进程
 父进程通过一个signal调用来扑获SIGALRM信号的工作，然后等待子进程5s后信号的到来。
 其中，用到了一个新函数pause,函数作用：等待程序的执行挂起知道一个信号出现为止。其函数原型：int pause(void);

 如果你开始在之间的程序中使用信号，就需要注意一些信号调用会因为接受到一个信号而失败，而这种错误可能是你在添加信号处理函数之前没有考虑到。
 在编写程序中处理信号部分的代码时必须非常小心，因为在使用信号的程序中会出现各种各样的“竟态条件”


 * */

    
