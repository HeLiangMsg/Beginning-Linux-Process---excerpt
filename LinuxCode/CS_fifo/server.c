#include "client.h"
#include <ctype.h>

int main()
{
    int serv_fifo_fd,client_fifo_fd;//两个fd，用于管道打开的返回值
    struct data_to_pass_st my_data;//一个结构体，
    int read_res;
    char client_fifo[256];
    char * tmp_char_ptr;

    mkfifo(SERVER_FIFO_NAME,0777);//创建服务器的管道
    serv_fifo_fd = open(SERVER_FIFO_NAME,O_RDONLY);//以堵塞的方式打开管道
    if (serv_fifo_fd == -1){
        perror("Server filo failure");
        exit(EXIT_FAILURE);
    }
//  sleep(9);//等待客户端队列?

    printf("Server FIFO is opened\n");

    do{
        read_res = read(serv_fifo_fd,&my_data,sizeof(my_data));//读取管道中的数据,一次读取一个结构体的数据大小
        if (read_res > 0) {
                tmp_char_ptr = my_data.some_data;//用一个指针指向这个结构体中的字符数组。
                while (*tmp_char_ptr) {
                    *tmp_char_ptr = toupper(*tmp_char_ptr);
                    tmp_char_ptr ++;
                }//遍历这个字符数组
                sprintf(client_fifo,CLIENT_FIFO_NAME,my_data.client_pid);//得到结构体中客户端的id

                printf ("client_id:[%s]\n",client_fifo);
                client_fifo_fd = open(client_fifo,O_WRONLY);//打开客户端的管道
                if (client_fifo_fd != -1) {
                    write(client_fifo_fd,&my_data,sizeof(my_data));//再将数据写回去，通过客户端的管道
                    printf("server open client_fifo success\n");;
                    sleep(2);//暂停2s等待客户端，读取管道中的数据。
                    close(client_fifo_fd);
                }else{
                    perror("open client fifo failure");
                }
        } 
    }while(read_res > 0);//使用read读取管道的返回值，作为循环的条件。
    printf("closeing serv_fifo \n");
    close(serv_fifo_fd);
    unlink(SERVER_FIFO_NAME);//从文件系统中删除一个名称。如果名称是文件的最后一个连接，并且没有其它进程将文件打开，名称对应的文件会实际被删除。
    exit(EXIT_SUCCESS);
}
