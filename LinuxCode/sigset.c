#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>

void handler(int sig)
{
	printf("Handle the signal %d\n", sig);
}

int main()
{
	sigset_t sigset;//用于记录屏蔽字
	sigset_t ign;//用于记录被阻塞的信号集
	struct sigaction act;

	//清空两个信号集
	sigemptyset (&sigset);
	sigemptyset (&ign);

	//添加一个信号到信号集中
	sigaddset(&sigset, SIGINT);
	
	//设置信号的处理函数
	act.sa_handler = handler;
	sigemptyset(&act.sa_mask);//??????
	act.sa_flags = 0;
	sigaction(SIGINT, &act, 0);

	printf("Wait he signal SIGNIT...\n");;
	pause();

	sigprocmask(SIG_SETMASK, &sigset, 0);
	printf("Please press Ctrl+c in 10 seconds....\n");
	sleep(10);
	sigpending(&ign);
	if (sigismember(&ign, SIGINT)) {
		printf("This SIGINT signal has ignored\n");;
	}
	sigdelset(&sigset, SIGINT);
	printf("Wait the signal SIGINT...\n");
	sigsuspend(&sigset);
	printf("The app waill exit is 5 seconds\n");

	exit(0);

}

