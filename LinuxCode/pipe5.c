#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h> 
#include <unistd.h>

int main()
{
    int data_processed;
    int file_pipe[2];
    const char some_data[] = "123";
    char buffer[BUFSIZ + 1];
    pid_t fork_result;
    /*
     * 一个文件描述符的数组，一个字符数组，一个buffer缓冲区，一个pid_t
     * */

    memset(buffer,'\0',sizeof(buffer));

    if (pipe(file_pipe) == 0){
        fork_result = fork();
        if(fork_result == (pid_t)-1){
            perror("fork");
            exit(EXIT_FAILURE);
        }

        if (fork_result == 0){
		close(0);
		dup(file_pipe[0]);	//把与管道的读取端关联的文件描述符复制为文件描述符0，子进程要从管道的读取端来读取数据。写数据交给父进程完成。

		//由于子进程对管道无法输入输出，则关闭子进程的输入输出管道描述符。
		close(file_pipe[0]);
		close(file_pipe[1]);

            execlp("od","od","-c",NULL);
            exit (EXIT_FAILURE);
        }else{
		//父进程只需要向管道写入数据，关闭管道的读取端。
		close(file_pipe[0]);
            data_processed = write(file_pipe[1],some_data,strlen(some_data));
	    close(file_pipe[1]);//向管道写完数据后关闭写入端的管道文件描述符
            printf("%d - wrote %d bytes \n",getpid(),data_processed);
        }
    }
    exit(EXIT_SUCCESS);

}

