#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void * start_routine(void * arg);

char message[] = "Hello World";

int main()
{
    int res;
    pthread_t a_thread;
    void * thread_result;

    res = pthread_create(&a_thread,NULL,start_routine,(void*) message);
    if (res != 0){
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }
    printf("Waiting for pthread to finish..\n");
    res = pthread_join(a_thread,&thread_result);//用于等待子线程的结束，arg1：指定子线程的标识符，arg2：子线程的返回值。即pthread_exit的参数
    if (res != 0){
            perror("Thread join failed");
            exit(EXIT_FAILURE);
    }
    printf("Thread joined,it returned [%s]\n",(char *)thread_result);
    printf("Message is now [%s]\n",message);
    exit(EXIT_SUCCESS);
}

void * start_routine(void * arg)
{
    printf("child Thread is running ,Argument was [%s]\n",(char*)arg);
    sleep(3);
    strcpy(message,"Bye!");
    pthread_exit("Cpu time");
}
