#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <sys/msg.h>

#define MAX_TEXT 512

#define MSG_EXCEPT      020000  /* recv any msg except of specified type.*/ 
struct my_msg_st {
	long int my_msg_type;
	char some_text[BUFSIZ];
};


int main() 
{
	int running = 1;
	int msgid;
	struct my_msg_st some_data;
	char buffer[BUFSIZ];
	
//	msgid = msgget((key_t) 1234, 0666/* | IPC_CREAT*/);
	msgid = msgget((key_t) 1234, 0666 | IPC_CREAT);
	if (msgid == -1) {
		fprintf (stderr,"msgget failed with errno:[%d]\n",errno);
		exit (EXIT_FAILURE);
	}

	while (running) {
		printf ("Enter some text: ");
		fgets(buffer,BUFSIZ,stdin);
		some_data.my_msg_type = 1;  //发送指定类型的数据
		strcpy(some_data.some_text,buffer);

//		if (msgsnd(msgid, (void*)&some_data,MAX_TEXT,MSG_EXCEPT ) == -1) {
		if (msgsnd(msgid, (void*)&some_data,BUFSIZ,0) == -1) {
			perror("msgsnd failed");
			exit (EXIT_FAILURE);
		}
		if (strncmp(buffer,"end",3) == 0) {
			running = 0;
		}
	}
	exit(EXIT_SUCCESS);

}
