#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h> 
#include <unistd.h>

int main()
{
    int data_processed;
    int file_pipe[2];
    const char some_data[] = "123";
    char buffer[BUFSIZ + 1];
    pid_t fork_result;
    /*
     * 一个父线程读取字节数， 一个文件描述符的数组，一个字符数组，一个buffer缓冲区，一个pid_t
     * */

    memset(buffer,'\0',sizeof(buffer));

    if (pipe(file_pipe) == 0){
        fork_result = fork();
        if(fork_result == (pid_t)-1){
            perror("fork");
            exit(EXIT_FAILURE);
	} else if (fork_result == 0){
            sprintf(buffer,"%d",file_pipe[0]);
            if (execl("./pip4","pip4",buffer,NULL) < 0) {
	    	perror("execl");
		exit(EXIT_FAILURE);
	    }
            exit (EXIT_FAILURE);
        }else{
		close(file_pipe[0]);
            data_processed = write(file_pipe[1],some_data,strlen(some_data));
            printf("%d - wrote %d bytes \n",getpid(),data_processed);
	    close(file_pipe[1]);
        }
    }


    exit(EXIT_SUCCESS);

}



/*
 * 在父进程写一次，让关闭管道一端
 * 在子进程中读两次，中间间隔1s，每次读取查看读取到的字节数
 * */
