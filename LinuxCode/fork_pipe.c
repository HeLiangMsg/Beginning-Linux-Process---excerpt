#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h> 
int main()
{
	int pipe_fd[2];
	pid_t pid;
	char r_buf[100];
	char w_buf[100];
	int r_num;
	
	memset(r_buf,0,sizeof(r_buf));
	memset(w_buf,0,sizeof(r_buf));
	/*
	 * 初始化两个数组
	 * */

	if(pipe(pipe_fd)<0)
	{
		printf("pipe create error\n");
		return -1;
	}
	
	if((pid=fork())==0)
	{
		printf("\n");
		close(pipe_fd[1]);//关闭子进程的写端
		sleep(3);//确保父进程关闭写端
	        r_num=read(pipe_fd[0],r_buf,50);
		printf(	"read num is %d   the data read from the pipe is %d\n",r_num,atoi(r_buf));
	        r_num=read(pipe_fd[0],r_buf,50);
		printf(	"read num is %d  \n",r_num );
		
		close(pipe_fd[0]);
		exit(0);
	}
	else if(pid>0)
	{
		close(pipe_fd[0]);//read
		strcpy(w_buf,"111");
		if(write(pipe_fd[1],w_buf,10)!=-1)
			printf("parent write over\n");
		close(pipe_fd[1]);//write
		printf("parent close fd[1] over\n");
		sleep(6);
	}else{
		perror("fork");
		exit(1);
	}	
}
