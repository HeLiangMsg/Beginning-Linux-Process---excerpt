#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc,char * argv[])
{
    char * filename;

    if(argc < 2){
        fprintf(stderr,"to few argument\n");
        exit(1);
    }

    filename = argv[1];

    if(!freopen(filename,"r",stdin)){
        fprintf(stderr,"Could not redirect stdin form file %s\n",filename);
        exit(2);
    }

    execl("./upper","upper",NULL);

    perror("Could not exec ./upper");

    exit(3);
}
