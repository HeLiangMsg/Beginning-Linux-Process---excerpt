#include <signal.h>
#include <stdio.h>
#include <unistd.h>

void outch(int sig)
{
    printf("OUCH! -I got signal %d\n",sig);
    signal(SIGINT,SIG_IGN);
}

int main()
{

    signal (SIGINT,outch);
    while(1){
        printf("no SIGNIT\n");
        sleep(1);
    }
}

