#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

#define MSGSZ	128

/*
 * Declare the message struct 
 * */

typedef struct msgbuf {
	long mtype;
	char mtext[MSGSZ];
} message_buf;

int main()
{
	int msqid;
	key_t key;
	message_buf rbuf;

	/*
	 * 得到这个消息队列通过“name”1234
	 *
	 * */
	key = 1234;

/*	if ((msqid = msgget(key,0666)) < 0); {
		perror("msget");
		printf("%d\n",msqid);
		exit(EXIT_FAILURE);
	}
*/
	msqid = msgget(key,0666);
	if (msqid < 0) {
		perror("msget");
		printf("%d\n",msqid);
		exit(EXIT_FAILURE);
	}
	/*
	 * 接收消息类型为1的回话
	 * */
/*	if (msgrcv(msqid,&rbuf,MSGSZ,1,0) < 0) {
		perror("msgrcv");
		exit(EXIT_FAILURE);
	}*///标识符id，当前进程的数据缓冲结构，大小，类型，0（表示忽略标志）
//	int result = msgrcv(msqid,&rbuf,MSGSZ,1,0);
	int result = msgrcv(msqid,&rbuf,MSGSZ,1,0);	
	if (result < 0) {
		perror("msgrcv");
		exit(EXIT_FAILURE);
	}

	/*
	 * 打印这个回话
	 * */

	printf("%s\n",rbuf.mtext);
	exit(EXIT_SUCCESS);


}


