#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define FIFO_NAME "/tmp/my_fifo"
#define TEM_MSG (1024*1024*10)

int main()
{
    int pipe_fd;
    int res;
    int open_mode = O_WRONLY;
    int bytes_sent = 0;
    char buffer[PIPE_BUF +1];

    if (access(FIFO_NAME,F_OK) == -1){
        res = mkfifo(FIFO_NAME,0777);//前面的0应该是8进制的标志吧？
        if (res != 0){
//            fprintf();
              char str[BUFSIZ];  
              sprintf(str,"Could not create fifo%s\n",FIFO_NAME);  
              perror(str);
              exit(EXIT_FAILURE);
        }
    }//判断目标管道文件是否存在
    printf("process [%d] opening FIFO O_WRONLY\n",getpid());//当前进程要打开管道
    pipe_fd = open(FIFO_NAME,open_mode);

    printf("Process [%d] open FIFO result [%d]\n",getpid(),pipe_fd);//当前进程打开管道的返回值

    if (pipe_fd != -1){
        while(bytes_sent < TEM_MSG) {
            res = write(pipe_fd,buffer,PIPE_BUF);
            if (res == -1) {
              perror("write failure");
              exit(EXIT_FAILURE);
            }
            bytes_sent += res;
        }
        close(pipe_fd);
    }else{
        exit(EXIT_FAILURE);
    }

    printf("Process [%d] finished\n",getpid());
    exit(EXIT_SUCCESS);
    
}











