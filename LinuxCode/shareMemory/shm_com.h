#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


#define TEXT_SZ 2048

struct share_use_st {
	int written_by_you;
	char some_text[TEXT_SZ];
};
