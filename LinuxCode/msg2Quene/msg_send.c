#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct msgbuf {
	long mtype;
	char mtex[BUFSIZ];
};

int main()
{
	int msgid,result;
	struct msgbuf myMsg;

//	msgid = msgget((key_t)2255,0666 | IPC_CREAT);
	msgid = msgget((key_t)3366, 0666|IPC_CREAT );
	if (msgid < 0) {
		perror("msgget");
		exit(1);
	}

	myMsg.mtype = 2;
	strcpy(myMsg.mtex,"this is Num2");
	msgsnd(msgid,(const void *)&myMsg ,BUFSIZ,0);

	myMsg.mtype = 1;
	strcpy(myMsg.mtex,"this is Num1");
	msgsnd(msgid,(const void *)&myMsg ,BUFSIZ,0);

	exit(0);


}
