#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct msgbuf {
	long mtype;
	char mtex[BUFSIZ];
};

int main()
{
	int result,msgid;
	struct msgbuf buf;
	long int msgtype ;

	msgid = msgget((key_t)3366,0666  | IPC_CREAT);
	msgtype = 2;
	result = msgrcv(msgid, (void*)&buf, BUFSIZ, msgtype, 0);
	if (result == -1) {
		perror("msgrcv");
		exit(1);
	}
	printf("recv data [%s]\n",buf.mtex);
	result = msgctl(msgid,IPC_RMID,0);
	if (result == -1) {
		perror("msgrcv");
		exit(1);
	}
	exit(0);
}



/**
 * 在编写消息队列时遇到的问题。
	 * １．权限不足，查看了自己的参数是正确，最后换了一个key_t的值，为了方便，直接使用的自定义数字。
	 * ２．非法参数。
		 * 在msgsnd时，Invalid argument，查看参数，long mtype的值必须大于0，消息的长度为实际发送数据的大小，不包含long type的大小。
		 * 在msgrcv时，同样消息的长度要确定。最后一个参数不知道怎么用就不要使用，补0
		 *
	 * 3. 数值交互
		 * 使用sigsnd发送指定类型数据时，需要指定long type的值，且值必须大于0.
		 * 使用sigrcv接受指定类型数据时，也需要指定long type的值。值任任意。可正可负，可为0
	 * 
 * */
